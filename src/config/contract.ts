export const CONTRACT_TYPE: Record<ContractType, ContractType> = {
  CMID: 'CMID',
  CMDFA: 'CMDFA',
  CMNFA: 'CMNFA',
  OTHER: 'OTHER',
  GAS: 'GAS',
  CMEVI: 'CMEVI',
};

export const CONTRACT_TYPE_REPLACE_MAP: Record<string, ContractType> = {
  FT: CONTRACT_TYPE.CMDFA,
  NFT: CONTRACT_TYPE.CMNFA,
};
