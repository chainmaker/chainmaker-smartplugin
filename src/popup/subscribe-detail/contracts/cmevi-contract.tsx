/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import React, { CSSProperties, memo, useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { DetailPage, CancelSubscribe } from '../../../utils/common';
import { Status } from 'tea-component';

import { FixedSizeList } from 'react-window';
import { contractStoreUtils, SubscribeContractItem } from '../../../utils/storage';
import { useChainStore } from '../../popup';
import { EviTxsItem } from '../../../components/txs-item';

const Row = (list, contract, browserLink) =>
  // eslint-disable-next-line react/display-name
  memo(({ index, style }: { index: number; style: CSSProperties }) => {
    const item = list[index];
    const navigate = useNavigate();
    return (
      <div key={index} style={style}>
        <EviTxsItem
          {...item}
          {...contract}
          copyable={false}
          onClick={() =>
            navigate('/subscribe-contract/transaction-detail', {
              state: {
                txInfo: item,
                contractInfo: contract,
                browserLink,
              },
            })
          }
        />
      </div>
    );
  });

export function CMEVIContractPage() {
  const location = useLocation();
  const contractInfo = location.state as SubscribeContractItem & { balance: string };
  const [txList, setTxsList] = useState([]);
  const { contractName } = contractInfo;
  const { selectedChain, currentAccount } = useChainStore();
  const accountId = currentAccount?.address;
  const chainId = selectedChain?.chainId;
  const browserLink = selectedChain?.browserLink;

  useEffect(() => {
    // 查询合约余额
    if (!accountId) return;
    // 查询转账交易
    contractStoreUtils
      .getContractTxs({
        chainId,
        contractName,
        accountId,
      })
      .then((res) => {
        setTxsList(res);
      });
  }, [accountId, contractName]);

  return (
    <DetailPage title={'合约详情'} className="free-width" backUrl={'/'}>
      <div className="txs-list">
        {txList.length === 0 ? (
          <Status icon={'blank'} size={'l'} className="cancel-bold" title={'暂无存证记录'} />
        ) : (
          <FixedSizeList
            height={430}
            itemCount={txList.length}
            itemSize={77}
            width={'100%'}
            className={'txlogs-vtable'}
          >
            {Row(txList, contractInfo, browserLink)}
          </FixedSizeList>
        )}
      </div>
      <CancelSubscribe chainId={chainId} contractName={contractName}></CancelSubscribe>
    </DetailPage>
  );
}
