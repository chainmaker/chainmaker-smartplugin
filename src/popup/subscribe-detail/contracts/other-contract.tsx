/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import React, { CSSProperties, memo, useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { DetailPage, CancelSubscribe } from '../../../utils/common';
import { Status } from 'tea-component';

import { FixedSizeList } from 'react-window';
import { contractStoreUtils, SubscribeContractItem } from '../../../utils/storage';
import { useChainStore } from '../../popup';
import { TxsItem } from '../../../components/txs-item';
import { getBrowserTransactionLink } from '../../../config/chain';

const Row = (logs, contract, browserLink, chainId) =>
  // eslint-disable-next-line react/display-name
  memo(({ index, style }: { index: number; style: CSSProperties }) => {
    const item = logs[index];

    return (
      <div key={index} style={style}>
        <TxsItem
          {...item}
          {...contract}
          href={browserLink && getBrowserTransactionLink({ browserLink, txId: item.txId, chainId })}
        />
      </div>
    );
  });

export function OtherContractPage() {
  const location = useLocation();
  const contractInfo = location.state as SubscribeContractItem;
  const [txList, setTxsList] = useState([]);
  const { contractName } = contractInfo;
  const { selectedChain, currentAccount } = useChainStore();
  const accountId = currentAccount?.address;
  const chainId = selectedChain?.chainId;
  const browserLink = selectedChain?.browserLink;

  useEffect(() => {
    if (!accountId) return;
    contractStoreUtils
      .getContractTxs({
        chainId,
        contractName,
        accountId,
      })
      .then((res) => {
        setTxsList(res);
      });
  }, [accountId, contractName]);

  return (
    <DetailPage title={'合约详情'} backUrl={'/'}>
      <div className="tx-list">
        {txList.length === 0 ? (
          <Status icon={'blank'} size={'l'} title={'暂无交易记录'} className="cancel-bold" />
        ) : (
          <FixedSizeList
            height={430}
            itemCount={txList.length}
            itemSize={77}
            width={'100%'}
            className={'txlogs-vtable'}
          >
            {Row(txList, contractInfo, browserLink, chainId)}
          </FixedSizeList>
        )}
      </div>
      <CancelSubscribe chainId={chainId} contractName={contractName}></CancelSubscribe>
    </DetailPage>
  );
}
