import React from 'react';
import { useLocation } from 'react-router-dom';
import { CONTRACT_TYPE } from '../../config/contract';
import { SubscribeContractItem } from '../../utils/storage';

import { DfaDetailPage } from './cmdfa-history-detail';
import { NfaDetailPage } from './cmnfa-history-detail';

export function TransactionHistoryDetailPage(props) {
  const location = useLocation();
  const { contractInfo } = location.state as { contractInfo: SubscribeContractItem; txInfo: any };
  const { contractType } = contractInfo;
  switch (contractType) {
    case CONTRACT_TYPE.CMDFA:
      return <DfaDetailPage {...props} />;
    case CONTRACT_TYPE.CMNFA:
      return <NfaDetailPage {...props} />;
    default:
      return <>交易历史</>;
  }
}
