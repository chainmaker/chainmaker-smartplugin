/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { Button, Form, Input, Icon, Select } from 'tea-component';
import { Controller, useForm, useWatch } from 'react-hook-form';
import { DetailPage, SignatureConfirmModal } from '../../utils/common';
import { useLocation, useNavigate } from 'react-router-dom';
import { invokeContract, preCheckForSignature } from '../../utils/utils';
import { useChainStore } from '../popup';
import GlossaryGuide from '../../utils/glossary-guide';
import chainStorageUtils, { NFTMetaData } from '../../utils/storage';
import { getBalanceGas, queryForecastGas } from '../../services/gas';
import formUtils from '../../utils/form-utils';
import { CONTRACT_TYPE } from '../../config/contract';
import { getAccountAddressByBNS } from '../../services/bns';
import { getFATransferContractContent } from '../../services/cmdfa';
import { getNFATransferContractContent } from '../../services/cmnfa';
import { isAccountAddress } from '@src/utils/tools';

const QUERY_STATUS = {
  NULL: 'null',
  LOADING: 'loading',
  ERROR: 'error',
  SUCCESS: 'success',
};
enum ToAccountType {
  BNS,
  ADDRESS,
}
const toAccountOptions = [
  {
    value: `${ToAccountType.ADDRESS}`,
    text: '账户地址',
  },
  {
    value: `${ToAccountType.BNS}`,
    text: '账户BNS',
  },
];
const isNotToMe = (contractType, toAccount, currentAccount) =>
  contractType === CONTRACT_TYPE.CMNFA && toAccount === currentAccount;

function TransferPage() {
  const location = useLocation();
  const {
    contractName = 'CMDFA',
    contractType = CONTRACT_TYPE.CMDFA,
    tokenId = '123',
    metadata,
    FTPerName = 'bt',
    balance = 100,
    prevPath = '/',
    prevState = {},
  } = (location.state as {
    contractName: string;
    balance?: number;
    tokenId?: string;
    metadata?: NFTMetaData;
    FTPerName?: string;
    contractType: ContractType;
    prevPath: string;
    prevState: any;
  }) || {};
  const navigate = useNavigate();
  const {
    control,
    setValue,
    formState: { isValid, isValidating, isSubmitted },
    resetField,
  } = useForm({
    mode: 'onBlur',
    reValidateMode: 'onBlur',
  });
  const toAccount: string = useWatch({ name: 'toAccount', control });
  const amount: string = useWatch({ name: 'amount', control });
  const gasLimit: string = useWatch({ name: 'gasLimit', control });
  const { selectedChain, currentAccount } = useChainStore();

  const [toAddress, setToAddress] = useState('');
  const [loading, setLoading] = useState(false);
  const [queryGasStatus, setQueryGasStatus] = useState(QUERY_STATUS.NULL);
  const [forecastGas, setForecastGas] = useState(null); // 兼容系统合约不返回gasuse
  const [gasBalance, setGasBalance] = useState(100);
  const [accountType, setAccountType] = useState(toAccountOptions[0].value);
  const [notAllowToMe, setNotAllowToMe] = useState(false);
  const [isInvalidAddress, setIsInvalidAddress] = useState(false);
  const signatureConfirmRef = useRef();

  const isBNS = useMemo(() => accountType === String(ToAccountType.BNS), [accountType]);

  useEffect(() => {
    (async function () {
      if (!selectedChain?.enableGas) return;
      const balance = await getBalanceGas({ chainId: selectedChain.chainId, account: currentAccount });
      setGasBalance(balance);
    })();
  }, []);

  const getToAddress = useCallback(
    (toAccount, amount) => {
      setToAddress('');
      setIsInvalidAddress(false);
      setNotAllowToMe(false);
      if (isBNS) {
        (async function () {
          try {
            const address = await getAccountAddressByBNS({
              chainId: selectedChain.chainId,
              account: currentAccount,
              domain: toAccount,
            });
            if (!isAccountAddress(address)) {
              throw new Error('bns resolve not address');
            }
            if (isNotToMe(contractType, address, currentAccount.address)) {
              setNotAllowToMe(true);
              return setIsInvalidAddress(true);
            }
            setToAddress(address);
            getForecastGas(amount, address);
          } catch (error) {
            setIsInvalidAddress(true);
            console.log(error);
          }
        })();
      } else if (isAccountAddress(toAccount)) {
        if (isNotToMe(contractType, toAccount, currentAccount.address)) {
          setNotAllowToMe(true);
          return setIsInvalidAddress(true);
        }
        setToAddress(toAccount);
        getForecastGas(amount, toAccount);
      } else {
        setIsInvalidAddress(true);
      }
    },
    [selectedChain, currentAccount, isBNS, gasBalance],
  );

  // 当区块链与账户已选择，且支持gas时，进行query
  const getForecastGas = useCallback(
    (amount, toAddress) => {
      const isValidAmount = Number(amount) && Number(amount) <= balance;
      console.log(isValidAmount, amount, Number(amount));
      if (toAddress && (contractType === CONTRACT_TYPE.CMNFA || isValidAmount)) {
        (async () => {
          if (selectedChain?.enableGas) {
            resetField('gasLimit');
            setQueryGasStatus(QUERY_STATUS.LOADING);
            // setGasBalance(0);
            setForecastGas(null);
            try {
              // 查预计消耗
              const contractContent =
                contractType === CONTRACT_TYPE.CMDFA
                  ? getFATransferContractContent({
                      contractName,
                      amount: Number(amount),
                      to: toAddress,
                    })
                  : getNFATransferContractContent({
                      contractName,
                      from: currentAccount.address,
                      tokenId,
                      metadata,
                      to: toAddress,
                    });
              const { gasUsed = 0 } = await queryForecastGas(
                selectedChain.chainId,
                currentAccount,
                contractContent,
                'invoke',
              );
              setForecastGas(gasUsed);
              if (gasUsed > gasBalance) {
                // 余额不足，无法交易
                // message.warning({ content: '账号gas余额不足，无法进行交易' });
              } else if (gasUsed * 1.1 > gasBalance) {
                // 余额不充足
                setValue('gasLimit', gasBalance, { shouldValidate: true });
              } else {
                setValue('gasLimit', Math.ceil(gasUsed * 1.1), { shouldValidate: true });
              }
              setQueryGasStatus(QUERY_STATUS.SUCCESS);
            } catch (e) {
              setQueryGasStatus(QUERY_STATUS.ERROR);
            }
          }
        })();
      }
    },
    [gasBalance, contractType],
  );

  const requestContract = useCallback(async () => {
    try {
      setLoading(true);
      if (
        !(await preCheckForSignature(selectedChain)) ||
        (contractType === CONTRACT_TYPE.CMDFA && !amount) ||
        !toAddress
      ) {
        return;
      }
      const contractContent =
        contractType === CONTRACT_TYPE.CMDFA
          ? getFATransferContractContent({
              contractName,
              amount: Number(amount),
              to: toAddress,
            })
          : getNFATransferContractContent({
              contractName,
              from: currentAccount.address,
              tokenId,
              metadata,
              to: toAddress,
            });
      if (selectedChain.enableGas) {
        Object.assign(contractContent, { limit: { gasLimit: Number(gasLimit) } });
      }
      // _GAS_LIMIT: userContract.limit?.gasLimit || 20000000,
      const result = await invokeContract(selectedChain.chainId, currentAccount, contractContent);

      navigate(result ? '/transaction/history' : '/tx-logs');
      // backUrl={prevPath} backState={prevState}
      chainStorageUtils.setLastTransTime();
    } catch (e) {
    } finally {
      setLoading(false);
    }
  }, [gasLimit, amount, toAddress]);

  const onSubmit = useCallback(async () => {
    if (await chainStorageUtils.getLastTransTime()) {
      await requestContract();
    } else {
      // @ts-ignore
      signatureConfirmRef.current.show({
        confirm: requestContract,
      });
    }
  }, [requestContract]);

  // 启动gas的链， 预估消耗gas不小于余额， gas上限不小于预估消耗gas
  const disableGas =
    selectedChain?.enableGas && (forecastGas === null || forecastGas > gasBalance || +gasLimit < forecastGas);

  // eslint-disable-next-line no-nested-ternary
  const invalidAddressMsg = isBNS ? `未检测到与该BNS对应的链账户` : `请输入合法的账户`;
  const accountMsg = notAllowToMe ? '不可以转账给自己' : invalidAddressMsg;

  return (
    <DetailPage title={contractType === CONTRACT_TYPE.CMNFA ? '转让' : '转账'} backUrl={prevPath} backState={prevState}>
      <div className={'signature'}>
        <Form layout={'vertical'}>
          <Controller
            control={control}
            rules={{
              required: '请输入',
            }}
            name="toAccount"
            render={({ field, fieldState }) => (
              <Form.Item
                showStatusIcon={false}
                label={<GlossaryGuide title={'接收方地址'} />}
                status={
                  isInvalidAddress
                    ? 'error'
                    : formUtils.getStatus({
                        fieldState,
                        isValidating,
                        isSubmitted,
                      })
                }
                message={isInvalidAddress ? accountMsg : fieldState.error?.message}
                extra={isBNS && toAddress}
              >
                {/* 该BNS无效，请添加正确的BNS或链账户地址 */}
                <div className="flex">
                  <Select
                    appearance="button"
                    // className='select-btn'
                    value={accountType}
                    size="full"
                    options={toAccountOptions}
                    onChange={(val) => {
                      setAccountType(val);
                      setToAddress('');
                      setIsInvalidAddress(false);
                      setValue('toAccount', '');
                    }}
                    defaultValue="链账户"
                    matchButtonWidth
                    style={{ width: 'auto', marginRight: '-1px' }}
                  />
                  <Input
                    size={'full'}
                    placeholder="请输入链账户地址或链账户BNS "
                    {...field}
                    onBlur={() => {
                      console.log('blur');
                      field.onBlur?.();
                      getToAddress(toAccount, amount);
                    }}
                    style={{
                      position: 'relative',
                      marginLeft: ' -1px',
                    }}
                  />
                </div>
              </Form.Item>
            )}
          />
          {contractType === CONTRACT_TYPE.CMDFA && (
            <Controller
              control={control}
              rules={{
                validate(v) {
                  // if (Number(v) === 0) return '请输入转账金额';
                  if (Number(v) === 0 || /\D/.test(v)) return '请输入整数转账金额';
                  if (Number(v) > balance) return '转账金额超过余额，请重新输入';
                  return true;
                },
              }}
              name="amount"
              render={({ field, fieldState }) => (
                <Form.Item
                  showStatusIcon={false}
                  label={<GlossaryGuide title={'转账金额'} />}
                  status={fieldState.error ? 'error' : undefined}
                  message={
                    fieldState.error?.message
                    // Number(amount) > balance ? '转账金额超过余额，请重新输入' : fieldState.error && '请输入转账金额'
                  }
                  extra={
                    <div className="flex-space-between">
                      <p>余额：{balance}</p>
                    </div>
                  }
                >
                  <div className="position-relative">
                    <Input
                      className="transfer-input"
                      size={'full'}
                      placeholder="请输入转账金额"
                      {...field}
                      onBlur={() => {
                        field.onBlur?.();
                        getForecastGas(amount, toAddress);
                      }}
                    />
                    <div className="flex-center transfer-input__addon">
                      <Button
                        type="link"
                        onClick={() => {
                          setValue('amount', balance, { shouldValidate: true });
                          getForecastGas(balance, toAddress);
                        }}
                      >
                        MAX
                      </Button>
                      <span className="cross-line"></span>
                      <span>{FTPerName}</span>
                    </div>
                  </div>
                </Form.Item>
              )}
            />
          )}

          {selectedChain?.enableGas && (
            <Controller
              control={control}
              rules={{
                required: '请输入',
                min: forecastGas,
                max: gasBalance,
              }}
              name="gasLimit"
              render={({ field, fieldState }) => (
                <Form.Item
                  label={<GlossaryGuide title={'GAS消耗最大值限制'} />}
                  status={
                    queryGasStatus === QUERY_STATUS.ERROR
                      ? 'error'
                      : formUtils.getStatus({
                          fieldState,
                          isValidating,
                          isSubmitted,
                        })
                  }
                  message={
                    queryGasStatus === QUERY_STATUS.ERROR
                      ? '预估GAS消耗量失败，请检查交易信息是否有误'
                      : fieldState.error && 'GAS消耗最大值限制不能小于预计消耗，不能大于账户余额'
                  }
                  extra={
                    <div className="flex-space-between">
                      <p>账户余额: {gasBalance}</p>
                      <p className="flex">
                        <span>预计该交易GAS消耗: </span>
                        {queryGasStatus === QUERY_STATUS.LOADING ? (
                          <i className="ss-icon">
                            <Icon size="s" type="loading" />
                          </i>
                        ) : (
                          forecastGas || 0
                        )}
                      </p>
                    </div>
                  }
                >
                  <Input size={'full'} placeholder="请输入GAS消耗最大值限制" {...field} />
                </Form.Item>
              )}
            />
          )}
        </Form>
        <div className={'flex-grow'} />
        <div>
          <Button
            type={'primary'}
            className={'btn-lg'}
            disabled={!isValid || disableGas || isInvalidAddress}
            onClick={() => {
              onSubmit();
            }}
            loading={loading}
          >
            确认
          </Button>
        </div>
        <SignatureConfirmModal ref={signatureConfirmRef} />
      </div>
    </DetailPage>
  );
}

export default TransferPage;
