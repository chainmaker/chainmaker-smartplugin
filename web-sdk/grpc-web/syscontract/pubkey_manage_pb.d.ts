import * as jspb from 'google-protobuf'



export enum PubkeyManageFunction { 
  PUBKEY_ADD = 0,
  PUBKEY_DELETE = 1,
  PUBKEY_QUERY = 2,
}
