// source: syscontract/test.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global = (function() { return this || window || global || self || Function('return this')(); }).call(null);

goog.exportSymbol('proto.syscontract.TestContractFunction', null, global);
/**
 * @enum {number}
 */
proto.syscontract.TestContractFunction = {
  P: 0,
  G: 1,
  N: 2,
  D: 3
};

goog.object.extend(exports, proto.syscontract);
