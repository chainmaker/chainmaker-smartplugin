import * as jspb from 'google-protobuf'



export enum TransactionManagerFunction { 
  ADD_BLACKLIST_TX_IDS = 0,
  DELETE_BLACKLIST_TX_IDS = 1,
  GET_BLACKLIST_TX_IDS = 2,
}
